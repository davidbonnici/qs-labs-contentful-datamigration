using System;
using System.Collections.Generic;
using Contentful.Core.Models;

namespace Quickspin.GameContent.Models.Contentful
{
    public class GameProfile
    {
         public string Id { get; set; }
        
        public string Title { get; set; }
        
        public string Description { get; set; }
        
        public string ShortGameDescription { get; set; }

        public decimal Rtp { get; set; }
        
        public decimal Volatility { get; set; }
        
        public bool Tournaments { get; set; }
        
        public bool Challenges { get; set; }
        
        public bool FlexibleFreeRounds { get; set; }
        
        public bool Achievements { get; set; }
        
        public bool FeatureTrigger { get; set; }
        
        public bool AchievementRaces { get; set; }
        
        public decimal NumberOfRounds { get; set; }
        
        public bool MobileLandscapeMode { get; set; }
        
        public bool MobilePortraitMode { get; set; }
        
        public string DesktopTechnology {get;set;}

        public string VolatilityClassificationType { get; set; }
        
        public DateTime ReleaseDate { get; set; }
        
        public string AspectRatio { get; set; }
        
        public string EvaluationType { get; set; }
        
        public decimal EvaluationNumber { get; set; }
        
        public string EvaluationCustom { get; set; }
        
        public IList<string> Jurisdictions { get; set; }
        
        public IList<string> ReelLayout { get; set; }
        
        public decimal MinimalBetInformation { get; set; }
        
        public decimal MaxBetInformation { get; set; }
        
        public decimal BaseBetInformation { get; set; }
        
        public decimal DefaultBetInformation { get; set; }
        
        public decimal MaximumPayout { get; set; }
        
        public decimal ExpectedPayout { get; set; }
        
        public decimal OddsForABigWin { get; set; }
        
        public decimal ThresholdBigWin { get; set; }
        
        public string Theme { get; set; }
        
        public IList<string> InGameFeatures { get; set; }
        
        public IList<Asset> Posters { get; set; }
    }
}