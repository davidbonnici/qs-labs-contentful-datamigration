using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Quickspin.GameContent.Models.Marketplace
{
    public class Game
    {
        public string Id { get; set; }
        
        [JsonProperty("gameTitle")]
        public string Title { get; set; }
        
        [JsonProperty("gameDesc")]
        public string Description { get; set; }
        
        [JsonProperty("shortGameDesc")]
        public string ShortGameDescription { get; set; }

        public decimal Rtp { get; set; }
        
        public decimal Volatility { get; set; }
        
        public bool Tournaments { get; set; }
        
        public bool Challenges { get; set; }
        
        public bool FlexibleFreeRounds { get; set; }
        
        public bool Achievements { get; set; }
        
        public bool FeatureTrigger { get; set; }
        
        public bool AchievementRaces { get; set; }
        
        [JsonProperty("numOfRounds")]
        public decimal NumberOfRounds { get; set; }
        
        public bool MobileLandscapeMode { get; set; }
        
        public bool MobilePortraitMode { get; set; }
        
        public string DesktopTechnology {get;set;}

        [JsonProperty("volatility_classification_type")]
        public string VolatilityClassificationType { get; set; }
        
        public DateTime ReleaseDate { get; set; }
        
        public string AspectRatio { get; set; }
        
        [JsonProperty("evaluation")]
        public Evaluation Evaluation { get; set; }
        
        public IList<string> Jurisdictions { get; set; }
        
        public IList<string> ReelLayout { get; set; }
        
        [JsonProperty("betInfo")]
        public BetInformation BetInformation { get; set; }

        [JsonProperty("payout")]
        public Payout Payout { get; set; }
        
        [JsonProperty("bigWin")]
        public BigWin BigWin { get; set; }
        
        public string Theme { get; set; }
        
        public IList<string> InGameFeatures { get; set; }
        
        [JsonProperty("posters")]
        public IList<Poster> Posters { get; set; }
    }
}