namespace Quickspin.GameContent.Models.Marketplace
{
    public class Payout
    {
        public decimal Max { get; set; }
        
        public decimal Expected { get; set; }
    }
}