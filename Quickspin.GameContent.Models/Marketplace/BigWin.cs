namespace Quickspin.GameContent.Models.Marketplace
{
    public class BigWin
    {
        public decimal Odds { get; set; }
        
        public decimal Threshold { get; set; }
    }
}