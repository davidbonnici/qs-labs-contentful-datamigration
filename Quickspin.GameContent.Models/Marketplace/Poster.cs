using System;

namespace Quickspin.GameContent.Models.Marketplace
{
    public class Poster
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public string Game { get; set; }
        
        public string Type { get; set; }
        
        public int Width { get; set; }
        
        public int Height { get; set; }
        
        public string Url { get; set; }
    }
}