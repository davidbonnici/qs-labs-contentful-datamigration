namespace Quickspin.GameContent.Models.Marketplace
{
    public class Evaluation
    {
        public string Type { get; set; }
        
        public decimal Number { get; set; }
        
        public string Custom { get; set; }
    }
}