namespace Quickspin.GameContent.Models.Marketplace
{
    public class BetInformation
    {
        public decimal Min { get; set; }
        
        public decimal Max { get; set; }
        
        public decimal Base { get; set; }
        
        public decimal Default { get; set; }
    }
}