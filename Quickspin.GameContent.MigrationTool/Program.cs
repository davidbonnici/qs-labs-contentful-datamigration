﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using Contentful.Core;
using Contentful.Core.Models;
using Contentful.Core.Models.Management;
using Quickspin.GameContent.Models.Contentful;
using Quickspin.GameContent.Models.Marketplace;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;

namespace Quickspin.GameContent.MigrationTool
{
    internal static class Program
    {
        private const string MarketplaceUrlEndpoint = "https://marketplace.quickspin.io";
        private const string ContentTypeNameForProfile = "gameProfile";
        private const string DefaultLanguageCode = "en";
        private const string ManagementApiKey = "CFPAT-6Y1TWAeTU2SfOXbp8M0__Iw5Eeh7v2HTX7xaQinRBug";
        private const string SpaceId = "826c8d0p3wiv";
        
        private static void Main()
        {
            Console.WriteLine("Press any key to start");
            Console.ReadKey();
            var sw = new Stopwatch();
            sw.Start();
            
            Console.WriteLine("Migration Started...");
            
            //Get Games from Marketplace API
            
            Console.WriteLine("Retrieved games from Games MetaData Api... Started");
            var client = new RestClient(MarketplaceUrlEndpoint);
            client.UseNewtonsoftJson();
            var request = new RestRequest("games", DataFormat.Json);
            var response = client.Get<List<Game>>(request);
            Console.WriteLine("Retrieved games from Games MetaData Api... Completed");
            
            Console.WriteLine("Transforming Data... Started");
            
            //Transform Data Response to Contentful Compatibility
            var gameProfileList = new List<GameProfile>();
            foreach (var marketplaceGame in response.Data)
            {
                var gameProfile = new GameProfile
                {
                    Achievements = marketplaceGame.Achievements,
                    Challenges = marketplaceGame.Challenges,
                    Description = marketplaceGame.Description,
                    Id = marketplaceGame.Id,
                    Jurisdictions = marketplaceGame.Jurisdictions,
                    Posters = new List<Asset>()
                };
                foreach (var marketplacePoster in marketplaceGame.Posters)
                {
                    gameProfile.Posters.Add(new Asset()
                    {
                        Title =
                            $"{marketplacePoster.Name} poster: Dimensions [{marketplacePoster.Width}px X {marketplacePoster.Height}px]",
                        File = new File()
                        {
                            FileName = marketplacePoster.Name,
                            UploadUrl = marketplacePoster.Url,
                            Details = new FileDetails()
                            {
                                Image = new ImageDetails()
                                {
                                    Width = marketplacePoster.Width,
                                    Height = marketplacePoster.Height
                                }
                            }
                        }
                    });
                }

                gameProfile.Rtp = marketplaceGame.Rtp;
                gameProfile.Theme = marketplaceGame.Theme;
                gameProfile.Title = marketplaceGame.Title;
                gameProfile.Tournaments = marketplaceGame.Tournaments;
                gameProfile.Volatility = marketplaceGame.Volatility;
                gameProfile.AchievementRaces = marketplaceGame.AchievementRaces;
                gameProfile.AspectRatio = marketplaceGame.AspectRatio;
                gameProfile.DesktopTechnology = marketplaceGame.DesktopTechnology;
                gameProfile.EvaluationCustom = marketplaceGame.Evaluation.Custom;
                gameProfile.EvaluationNumber = marketplaceGame.Evaluation.Number;
                gameProfile.EvaluationType = marketplaceGame.Evaluation.Type;
                gameProfile.ExpectedPayout = marketplaceGame.Payout?.Expected ?? 0;
                gameProfile.FeatureTrigger = marketplaceGame.FeatureTrigger;
                gameProfile.MaximumPayout = marketplaceGame.Payout?.Max ?? 0;
                gameProfile.ReelLayout = marketplaceGame.ReelLayout;
                gameProfile.ReleaseDate = marketplaceGame.ReleaseDate;
                gameProfile.BaseBetInformation = marketplaceGame.BetInformation.Base;
                gameProfile.DefaultBetInformation = marketplaceGame.BetInformation.Default;
                gameProfile.FlexibleFreeRounds = marketplaceGame.FlexibleFreeRounds;
                gameProfile.InGameFeatures = marketplaceGame.InGameFeatures;
                gameProfile.MaxBetInformation = marketplaceGame.BetInformation.Max;
                gameProfile.MinimalBetInformation = marketplaceGame.BetInformation.Min;
                gameProfile.MobileLandscapeMode = marketplaceGame.MobileLandscapeMode;
                gameProfile.MobilePortraitMode = marketplaceGame.MobilePortraitMode;
                gameProfile.NumberOfRounds = marketplaceGame.NumberOfRounds;
                gameProfile.ShortGameDescription = marketplaceGame.ShortGameDescription;
                gameProfile.ThresholdBigWin = marketplaceGame.BigWin.Threshold;
                gameProfile.VolatilityClassificationType = marketplaceGame.VolatilityClassificationType;
                gameProfile.OddsForABigWin = marketplaceGame.BigWin.Odds;

                gameProfileList.Add(gameProfile);
            }

            Console.WriteLine("Transforming Data... Completed");
            
            
            Console.WriteLine("Uploading Data to Contentful... Started");

            //Migrate Content to Contentful
            var httpClient = new HttpClient();
            var managementClient = new ContentfulManagementClient(httpClient,ManagementApiKey, SpaceId);
            foreach (var gameProfile in gameProfileList)
            {
                var assetIdsForCurrentGame = new List<string>();
                
                foreach (var poster in gameProfile.Posters)
                {
                    var managementAsset = new ManagementAsset
                    {
                        SystemProperties = new SystemProperties(),
                        Title = new Dictionary<string, string> {{DefaultLanguageCode, gameProfile.Title},},
                        Files = new Dictionary<string, File>
                        {
                            {
                                DefaultLanguageCode,
                                new File()
                                {
                                    ContentType = "image/png",
                                    FileName = poster.File.FileName + ".png",
                                    UploadUrl = poster.File.UploadUrl
                                }
                            }
                        }
                    };

                    var createdAsset = managementClient.CreateAsset(managementAsset).Result;
                    managementClient.ProcessAsset(createdAsset.SystemProperties.Id,1, DefaultLanguageCode);

                    assetIdsForCurrentGame.Add(createdAsset.SystemProperties.Id);

                }
                
                
                var referenceList = assetIdsForCurrentGame.Select(id => new Reference(SystemLinkTypes.Asset, id)).ToList();


                var entry = new Entry<dynamic>
                {
                    SystemProperties = new SystemProperties {Id = gameProfile.Id},
                    Fields = new
                    {
                        Description = new Dictionary<string, string>() {{DefaultLanguageCode, gameProfile.Description}},
                        Id = new Dictionary<string, string>() {{DefaultLanguageCode, gameProfile.Id}},
                        Theme = new Dictionary<string, string>() {{DefaultLanguageCode, gameProfile.Theme}},
                        Title = new Dictionary<string, string>() {{DefaultLanguageCode, gameProfile.Title}},
                        AspectRatio = new Dictionary<string, string>() {{DefaultLanguageCode, gameProfile.AspectRatio}},
                        DesktopTechnology = new Dictionary<string, string>() {{DefaultLanguageCode, gameProfile.DesktopTechnology}},
                        EvaluationCustom = new Dictionary<string, string>() {{DefaultLanguageCode, gameProfile.EvaluationCustom}},
                        EvaluationType = new Dictionary<string, string>() {{DefaultLanguageCode, gameProfile.EvaluationType}},
                        ShortGameDescription = new Dictionary<string, string>()
                            {{DefaultLanguageCode, gameProfile.ShortGameDescription}},
                        VolatilityClassificationType = new Dictionary<string, string>()
                            {{DefaultLanguageCode, gameProfile.VolatilityClassificationType}},
                        Achievements = new Dictionary<string, bool>() {{DefaultLanguageCode, gameProfile.Achievements}},
                        Challenges = new Dictionary<string, bool>() {{DefaultLanguageCode, gameProfile.Challenges}},
                        Tournaments = new Dictionary<string, bool>() {{DefaultLanguageCode, gameProfile.Tournaments}},
                        AchievementRaces = new Dictionary<string, bool>() {{DefaultLanguageCode, gameProfile.AchievementRaces}},
                        FeatureTrigger = new Dictionary<string, bool>() {{DefaultLanguageCode, gameProfile.FeatureTrigger}},
                        FlexibleFreeRounds = new Dictionary<string, bool>() {{DefaultLanguageCode, gameProfile.FlexibleFreeRounds}},
                        MobileLandscapeMode = new Dictionary<string, bool>() {{DefaultLanguageCode, gameProfile.MobileLandscapeMode}},
                        MobilePortraitMode = new Dictionary<string, bool>() {{DefaultLanguageCode, gameProfile.MobilePortraitMode}},
                        Jurisdictions =
                            new Dictionary<string, string[]>() {{DefaultLanguageCode, gameProfile.Jurisdictions.ToArray()}},
                        Rtp = new Dictionary<string, decimal>() {{DefaultLanguageCode, gameProfile.Rtp}},
                        Volatility = new Dictionary<string, decimal>() {{DefaultLanguageCode, gameProfile.Volatility}},
                        EvaluationNumber = new Dictionary<string, decimal>() {{DefaultLanguageCode, gameProfile.EvaluationNumber}},
                        ExpectedPayout = new Dictionary<string, decimal>() {{DefaultLanguageCode, gameProfile.ExpectedPayout}},
                        MaximumPayout = new Dictionary<string, decimal>() {{DefaultLanguageCode, gameProfile.MaximumPayout}},
                        BaseBetInformation = new Dictionary<string, decimal>() {{DefaultLanguageCode, gameProfile.BaseBetInformation}},
                        DefaultBetInformation = new Dictionary<string, decimal>()
                            {{DefaultLanguageCode, gameProfile.DefaultBetInformation}},
                        MaxBetInformation = new Dictionary<string, decimal>() {{DefaultLanguageCode, gameProfile.MaxBetInformation}},
                        MinimalBetInformation = new Dictionary<string, decimal>()
                            {{DefaultLanguageCode, gameProfile.MinimalBetInformation}},
                        NumberOfRounds = new Dictionary<string, decimal>() {{DefaultLanguageCode, gameProfile.NumberOfRounds}},
                        ThresholdBigWin = new Dictionary<string, decimal>() {{DefaultLanguageCode, gameProfile.ThresholdBigWin}},
                        OddsForABigWin = new Dictionary<string, decimal>() {{DefaultLanguageCode, gameProfile.OddsForABigWin}},
                        ReleaseDate = new Dictionary<string, DateTime>() {{DefaultLanguageCode, gameProfile.ReleaseDate}},
                        Posters = new Dictionary<string, List<Reference>>()
                        {
                            {
                                DefaultLanguageCode, referenceList
                            }
                        }
                    }
                };
                Console.WriteLine("Preparing upload of a new game profile: " + entry.SystemProperties.Id);
                var result = managementClient.CreateOrUpdateEntry(entry, contentTypeId: ContentTypeNameForProfile).Result;
                Console.WriteLine("Completed: " + gameProfile.Id + " with Contentful Id: " + result.SystemProperties.Id);
            }

            Console.WriteLine("Uploading Data to Contentful... Finished");
            
           
            sw.Stop();
            Console.WriteLine("Completed in " + sw.Elapsed.Seconds + "seconds");
            Console.ReadKey();
        }
    }
}
