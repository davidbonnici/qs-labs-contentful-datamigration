﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Contentful.Core;
using Contentful.Core.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Quickspin.GameContent.Models.Contentful;
using Quickspin.GameContent.Models.Marketplace;

namespace Quickspin.GameContent.Mediator.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GamesController : ControllerBase
    {

        private readonly ILogger<GamesController> _logger;

        public GamesController(ILogger<GamesController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Game> Get()
        {
            var contentHttpClient = new HttpClient();
            var options = new ContentfulOptions {
                DeliveryApiKey = "FJJAgaktshK6XZH7-k60V_8n5XhSz5b-XVtY4xg5q20",
                PreviewApiKey = "MlgR5ODIwnmPDCVsFgjIM-cfdb7yRNIybbgNF7wUtS0",
                SpaceId = "826c8d0p3wiv"
            };
            var contentClient = new ContentfulClient(contentHttpClient, options);
            var data = contentClient.GetEntriesByType<GameProfile>("gameProfile").Result;

            var gameList = new List<Game>();
            
            foreach (var game in data)
            {
                var xGame = new Game
                {
                    Achievements = game.Achievements,
                    Challenges = game.Challenges,
                    Description = game.Description,
                    Evaluation =
                        new Evaluation()
                        {
                            Custom = game.EvaluationCustom,
                            Number = game.EvaluationNumber,
                            Type = game.EvaluationType
                        },
                    Id = game.Id,
                    Jurisdictions = game.Jurisdictions,
                    Payout = new Payout() {Expected = game.ExpectedPayout, Max = game.MaximumPayout},
                    Rtp = game.Rtp,
                    Theme = game.Theme,
                    Title = game.Title,
                    Tournaments = game.Tournaments,
                    Volatility = game.Volatility,
                    AchievementRaces = game.AchievementRaces,
                    AspectRatio = game.AspectRatio,
                    BetInformation =
                        new BetInformation()
                        {
                            Base = game.BaseBetInformation,
                            Default = game.DefaultBetInformation,
                            Max = game.MaxBetInformation,
                            Min = game.MinimalBetInformation
                        },
                    BigWin = new BigWin() {Odds = game.OddsForABigWin, Threshold = game.ThresholdBigWin},
                    DesktopTechnology = game.DesktopTechnology,
                    FeatureTrigger = game.FeatureTrigger,
                    ReelLayout = game.ReelLayout,
                    ReleaseDate = game.ReleaseDate,
                    FlexibleFreeRounds = game.FlexibleFreeRounds,
                    InGameFeatures = game.InGameFeatures,
                    MobileLandscapeMode = game.MobileLandscapeMode,
                    MobilePortraitMode = game.MobilePortraitMode,
                    NumberOfRounds = game.NumberOfRounds,
                    ShortGameDescription = game.ShortGameDescription,
                    VolatilityClassificationType = game.VolatilityClassificationType,
                    Posters = new List<Poster>()
                };

                foreach (var poster in game.Posters)
                {
                    
                    xGame.Posters.Add(new Poster()
                    {
                        Game = poster.Title,
                        Height = poster.File.Details.Image.Height,
                        Width = poster.File.Details.Image.Width,
                        Id = Guid.NewGuid(),
                        Url = poster.File.Url,
                        Type = poster.File.ContentType,
                        Name = poster.Title
                    });
                    
                }
                gameList.Add(xGame);
            }
            return gameList;
        }
    }
}